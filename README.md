# Soft3D {#soft3dPage}

This package is the SIBR (http://sibr.gitlabpages.inria.fr) version of P. Hedman's re-implementation of:

[Penner and Zhang 2017] Penner, Eric, and Li Zhang. "Soft 3D reconstruction for view synthesis." ACM Transactions on Graphics (TOG) 36, no. 6 (2017): 1-11.

(https://ericpenner.github.io/soft3d/)

The implementation is much slower than the original paper, and there is no guarantee on the quality of the results. Empirically, the quality is similar on the Museum-Front-27 scene however.

## Preprocessing a dataset

First run <code>colmap</code> on the dataset e.g., as explained in the SIBR documentation [fullcolmapPreprocess](https://sibr.gitlabpages.inria.fr/docs/develop/sibr_projects_dataset_tools_preprocess_tools_fullColmapProcess.html). Then run the following two refinement steps.

### Create a coarse reconstruction

```
> ./SIBR_soft3d_app_rwdi.exe --path [PATH_TO_DATASET] --batch_reconstruct 
```

### Refine the reconstruction

```
// SIBR Command
> ./SIBR_soft3d_app_rwdi.exe --path [PATH_TO_DATASET] --batch_reconstruct 
```

## Render images from the novel camera path

```
> ./SIBR_soft3d_app_rwdi.exe --path [PATH_TO_MUSEUM1] --batch_render --pathFile path.out
```
---

## How to use

The code has been developed for Windows (10), and we currently only support this platform. A Linux version will follow shortly.

### Use the binary distribution

The easiest way to use *SIBR* to run Soft3d is to download the binary distribution. All steps described above, i.e., preprocessing for your datasets and rendering will work using this code.
Download the executables, tools and libraries specific to *soft3d for SIBR* *Project* here:
```
wget https://repo-sam.inria.fr/fungraph/sibr-release/soft3d/install.zip
```
and unzip which will add the necessary files.

 
### Compiling and building the code

You will need to checkout SIBR Core. For this use the following commands:

```bash
## through HTTPS
git clone https://gitlab.inria.fr/sibr/sibr_core.git
## through SSH
git clone git@gitlab.inria.fr:sibr/sibr_core.git
```
Then clone the soft3d project
```bash
## through HTTPS
git clone https://gitlab.inria.fr/sibr/projects/soft3d.git
## through SSH
git clone git@gitlab.inria.fr:sibr/projects/soft3d.git
```

### Configuration
 As for most SIBR projects, soft3d can be configured through SIBR Core CMake configuration by selecting `SIBR_IBR_SOFT3D` variable before running the configuration (see \ref sibr_configure_cmake)  [Configuring the solution](https://sibr.gitlabpages.inria.fr/docs/develop/index.html#sibr_configure_cmake).
 
### Build & Install
 
You can build and install *soft3d* via running ALL_BUILD and/or INSTALL in sibr_projects.sln solution (as mentioned in \ref sibr_compile [compile page in the documentation](https://sibr.gitlabpages.inria.fr/docs/develop/index.html#sibr_compile) or through `*sibr_soft3d*` specific targets in sibr_projects.sln solution.
 Dont forget to build INSTALL if you use ALL_BUILD.
