#include "projects/fribr_framework/renderer/gl_wrappers.h"
#include "projects/fribr_framework/renderer/tools.h"
#include "projects/fribr_framework/renderer/io.h"
#include "projects/fribr_framework/renderer/3d.h"
#include "projects/fribr_framework/renderer/3dmath.h"

#include "core/video/FFmpegVideoEncoder.hpp"

#include "simple_ransac.h"

#include <core/system/CommandLineArgs.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/ximgproc.hpp>

#include <ctime>
#include <cmath>
#include <cfloat>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <functional>

using namespace Eigen;
using namespace fribr;
using namespace sibr;

#define DEBUG_STEREO 0


struct Soft3DIBRAppArgs :
	virtual BasicIBRAppArgs {
	Arg<float> fov_arg = { "fov", -1.0f };
	Arg<int> cam_step = { "step", 1 };
	Arg<sibr::Switch> do_interpolation = { "enable-interpolation", false };
	Arg<bool> batch_reconstruct = { "batch_reconstruct" };
	Arg<bool> batch_render = { "batch_render" };
	Arg<int> start_frame = { "start_frame", 0 };
	Arg<int> sampling_rate = { "sampling_rate", 30 };
	Arg<std::string> render_path = { "render_path", "bundle_path_soft3d.out" };
};

namespace
{

	void error_callback(int error, const char* description)
	{
		std::cerr << "Error " << error << " " << description << std::endl;
	}

	//
	// Depth map I/O.
	//

	cv::Mat1f load_depth(const std::string &filename)
	{
		std::ifstream header(filename, std::ios::in | std::ios::binary);
		if (!header)
		{
			std::cerr << "Could not load depth map header at: " << filename << std::endl;
			return cv::Mat1f();
		}

		char temp;
		size_t width, height, depth;
		header >> width >> temp >> height >> temp >> depth >> temp;

		std::streampos header_end = header.tellg();
		header.close();

		if (width < 0 || height < 0 || depth != 1)
		{
			std::cerr << "Invalid depth map format ("
				<< width << "x" << height << "x" << depth << ")"
				<< " for file: " << filename << std::endl;
			return cv::Mat1f();
		}

		cv::Mat1f depth_map((int)(height), (int)(width));
		std::ifstream data(filename, std::ios::in | std::ios::binary);
		if (!data)
		{
			std::cerr << "Could not load depth map data at: " << filename << std::endl;
			return cv::Mat1f();
		}
		data.seekg(header_end);
		data.read((char*)depth_map.ptr(), width * height * sizeof(float));
		data.close();

		return depth_map;
	}

	void save_depth(const std::string &filename, const cv::Mat1f &depth_map)
	{
		std::ofstream header(filename, std::ios::out);
		if (!header)
		{
			std::cerr << "Could not load depth map header for writing: " << filename << std::endl;
			return;
		}

		header << depth_map.cols << "&" << depth_map.rows << "&" << 1 << "&";
		header.close();

		std::ofstream data(filename, std::ios::out | std::ios::binary | std::ios::app);
		if (!data)
		{
			std::cerr << "Could not load depth map data for writing: " << filename << std::endl;
			return;
		}

		data.write((char*)depth_map.ptr(), depth_map.cols * depth_map.rows * sizeof(float));
		data.close();
	}

	bool scene_directory_exists(const std::string base_path, const std::string &folder)
	{
		const std::string folder_path = base_path + "/soft3D/" + folder;
		return directory_exists(folder_path);
	}

	std::vector<cv::Mat1f> load_depth_maps(const CalibratedImage::Vector &cameras, const std::string &folder, const std::string base_path)
	{
		std::vector<cv::Mat1f> depth_maps;
		depth_maps.reserve(cameras.size());

		for (const CalibratedImage &camera : cameras)
		{
			const std::string color_path = camera.get_image_path();
			const std::string color_name = get_filename(color_path);
			const std::string depth_path = base_path + "/soft3D/" + folder + "/" + color_name + ".bin";


			cv::Mat1f depth_map = load_depth(depth_path);
			if (depth_map.empty())
			{
				std::cerr << "Could not load depth map for image: " << color_name << " at: " << depth_path << std::endl;
				return std::vector<cv::Mat1f>(cameras.size());
			}

			depth_maps.emplace_back(depth_map);
		}

		return depth_maps;
	}

	void save_depth_maps(const CalibratedImage::Vector &cameras,
		const std::vector<cv::Mat1f> &depth_maps,
		const std::string &folder, const std::string base_path)
	{
		const std::string folder_path = base_path + "/soft3D/" + folder;
		const std::string s3d_folder_path = base_path + "/soft3D/";
		if (!directory_exists(s3d_folder_path))
			make_directory(s3d_folder_path);

		if (!directory_exists(folder_path))
			make_directory(folder_path);

		for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
		{
			const CalibratedImage &camera = cameras[i];
			const std::string color_name = get_filename(camera.get_image_path());
			const std::string depth_path = folder_path + "/" + color_name + ".bin";

			save_depth(depth_path, depth_maps[i]);
		}
	}

	void load_consensus_volumes(const CalibratedImage::Vector &cameras, const std::string &folder, const int depth_planes,
		std::vector<std::vector<cv::Mat1b>> &consensus_volumes, const std::string base_path)
	{
		consensus_volumes.clear();
		consensus_volumes.resize(cameras.size());

		bool fail = false;
		int progress = 0;
		std::cout << "\rLoading consensus volumes: "
			<< std::setfill('0') << std::setw(3)
			<< progress << "/"
			<< std::setfill('0') << std::setw(3)
			<< cameras.size() << std::flush;
#ifdef NDEBUG
#pragma omp parallel for schedule(dynamic)
#endif
		for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
		{
			if (fail)
				continue; // Break not allowed with openmp..

			const CalibratedImage &camera = cameras[i];
			const std::string      color_path = camera.get_image_path();
			const std::string      color_name = get_filename(color_path);
			const std::string      volume_path = base_path + "/soft3D/" + folder + "/" + color_name;

			consensus_volumes[i].clear();
			consensus_volumes[i].reserve(depth_planes);
			for (int j = 0; j < depth_planes; ++j)
			{
				if (fail)
					break;

				std::stringstream plane_stream;
				plane_stream << volume_path << "/" << std::setw(3) << std::setfill('0') << j << ".png";
				cv::Mat1b volume_plane = cv::imread(plane_stream.str(), cv::IMREAD_GRAYSCALE);
				if (volume_plane.empty())
				{
#pragma omp critical
					{
						std::cerr << std::endl << "Could not load consensus volume slice at: "
							<< plane_stream.str() << std::endl;
						fail = true;
					}
					break;
				}

				consensus_volumes[i].emplace_back(volume_plane);
			}

#pragma omp critical
			{
				progress++;
				std::cout << "\rLoading consensus volumes: "
					<< std::setfill('0') << std::setw(3)
					<< progress << "/"
					<< std::setfill('0') << std::setw(3)
					<< cameras.size() << std::flush;
			}
		}
		std::cout << std::endl;
	}

	void save_consensus_volumes(const CalibratedImage::Vector &cameras,
		std::vector<std::vector<cv::Mat1b>> &consensus_volumes,
		const std::string &folder, const std::string base_path)
	{
		const std::string folder_path = base_path + "/soft3D/" + folder;
		const std::string s3d_folder_path = base_path + "/soft3D/";
		if (!directory_exists(s3d_folder_path))
			make_directory(s3d_folder_path);

		if (!directory_exists(folder_path))
			make_directory(folder_path);

		int progress = 0;
		std::cout << "\rSaving consensus volumes: "
			<< std::setfill('0') << std::setw(3)
			<< progress << "/" << cameras.size() << std::flush;
#ifdef NDEBUG
#pragma omp parallel for schedule(dynamic)
#endif
		for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
		{
			const CalibratedImage &camera = cameras[i];
			const std::string      color_name = get_filename(camera.get_image_path());
			const std::string      volume_path = folder_path + "/" + color_name;
			if (!directory_exists(volume_path))
				make_directory(volume_path);

			for (int j = 0; j < static_cast<int>(consensus_volumes[i].size()); ++j)
			{
				std::stringstream plane_stream;
				plane_stream << volume_path << "/" << std::setw(3) << std::setfill('0') << j << ".png";
				cv::imwrite(plane_stream.str(), consensus_volumes[i][j]);
			}

#pragma omp critical
			{
				progress++;
				std::cout << "\rSaving consensus volumes: "
					<< std::setfill('0') << std::setw(3)
					<< progress << "/" << cameras.size() << std::flush;
			}
		}
		std::cout << std::endl;
	}

	void interpolate_camera_path(std::vector<Eigen::Matrix4f>& interpolated_world_to_camera, const CalibratedImage::Vector &cameras,
			const int sampling_rate, int startFrame = 0, int cam_step = 1, bool doInterpolation = true, bool bundler = false)
	{
		if (cameras.empty())
			return;
			//return std::vector<Eigen::Matrix4f>();

		Eigen::Vector3f average_up(0.0f, 0.0f, 0.0f);
		for (const CalibratedImage &camera : cameras)
			average_up += camera.get_orientation() * Eigen::Vector3f(0.0f, 1.0f, 0.0f);
		average_up.normalize();

		std::vector<Eigen::Vector3f> control_positions;
		std::vector<Eigen::Vector3f> control_directions;
		if (bundler) {
			if (doInterpolation) {
				// take evert 25th camera (parameter ?)
				for (int i = 0; i < static_cast<int>(cameras.size()); i += 25) {
					control_positions.emplace_back(cameras[i].get_position());
					control_directions.emplace_back(cameras[i].get_forward());
				}
			}
			else {
				for (int i = 0; i < static_cast<int>(cameras.size()); i ++ ) {
					control_positions.emplace_back(cameras[i].get_position());
					control_directions.emplace_back(cameras[i].get_forward());
				}
			}
		}
		else {
			// Sort the cameras according to their x-coordinates
			std::vector<int> indices;
			for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
				indices.push_back(i);
			std::sort(indices.begin(), indices.end(), [&](int a, int b)
			{
				return cameras[a].get_position().x() < cameras[b].get_position().x();
			});

			for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
			{
				int index = indices[i];
				control_positions.emplace_back(cameras[index].get_position());
				control_directions.emplace_back(cameras[index].get_forward());
			}
		}

		std::vector<Eigen::Vector3f> interpolated_positions = control_positions;
		std::vector<Eigen::Vector3f> interpolated_directions = control_directions;
		if (doInterpolation) {
			smooth_interpolate(control_positions, sampling_rate, &interpolated_positions);
			smooth_interpolate(control_directions, sampling_rate, &interpolated_directions);
		}

		//std::vector<Eigen::Matrix4f> interpolated_world_to_camera;
		for (int i = 0; i < static_cast<int>(interpolated_positions.size()); i += cam_step)
		{
			Eigen::Vector3f interpolated_position = interpolated_positions[i];
			Eigen::Vector3f interpolated_forward = interpolated_directions[i];
			Eigen::Matrix3f interpolated_orientation =
				look_at(Eigen::Vector3f(0.0f, 0.0f, 0.0f), interpolated_forward, average_up);


			if (bundler) {
				static int tcnt = 0;
				if (i >= startFrame) {
					if (doInterpolation) {
						interpolated_world_to_camera.emplace_back(
							compute_world_to_cam(interpolated_orientation, interpolated_position));
					}
					else {
						interpolated_world_to_camera.emplace_back(
							compute_world_to_cam(cameras[i].get_orientation(), cameras[i].get_position()));
					}

//					if (tcnt++ < 10)
//						std::cerr << " CAM " << i << " " << interpolated_position.x() << " " << interpolated_position.y() << " " << interpolated_position.z() << std::endl;
				}
			}
			else {

				interpolated_world_to_camera.emplace_back(
					compute_world_to_cam(interpolated_orientation, interpolated_position));
			}
		}

		//return interpolated_world_to_camera;
		return;
	}

	struct CommonParameters
	{
		int  max_depth_map_width = 1920;
		int  num_depth_planes = 128;
		bool use_sqrt_disparity = true;
	};

	cv::Mat1f compute_variance(const cv::Mat3b &image, int kernel_size)
	{
		cv::Mat3f imagef = image;
		imagef /= 255.0f;

		cv::Mat3f sqr_imagef = imagef.mul(imagef);

		cv::Mat3f meanf, sqr_meanf;
		cv::blur(imagef, meanf, cv::Size(kernel_size, kernel_size));
		cv::blur(sqr_imagef, sqr_meanf, cv::Size(kernel_size, kernel_size));

		cv::Mat3f variancef = sqr_meanf - meanf.mul(meanf);
		std::vector<cv::Mat1f> channel_variances(3);
		cv::split(variancef, channel_variances);

		return channel_variances[0] + channel_variances[1] + channel_variances[2];
	}

	cv::Mat4b add_alpha_channel(const cv::Mat3b &image)
	{
		cv::Mat rgb = image;
		cv::Mat alpha = cv::Mat1b::ones(image.size()) * 255;

		cv::Mat src[] = { rgb, alpha };
		int     from_to[] = { 0,0, 1,1, 2,2, 3,3 };

		cv::Mat4b alpha_image(image.size());
		cv::mixChannels(src, 2, &alpha_image, 1, from_to, 4);

		return alpha_image;
	}

	typedef std::pair<float, int> NeighborIndex;
	enum Strictness { DiscardExactMatches, ReturnAllCameras };
	std::vector<NeighborIndex> find_neighbors(const Eigen::Vector3f &position,
		const CalibratedImage::Vector &cameras,
		Strictness strictness)
	{
		std::vector<NeighborIndex> neighbors;
		for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
		{
			Eigen::Vector3f neighbor_position = cameras[i].get_position();
			if (strictness == DiscardExactMatches && position == neighbor_position)
				continue;

			float distance = (position - neighbor_position).norm();
			neighbors.emplace_back(std::make_pair(distance, i));
		}
		std::sort(neighbors.begin(), neighbors.end());
		return neighbors;
	}

	void estimate_near_and_far(CalibratedImage &camera,
		const PointCloud::Ptr &point_cloud,
		float &near_p, float &far_p)
	{
		static const float conservative_near_plane = 0.001f;
		static const float conservative_far_plane = 1e10f;

		// Return conservative defaults if the point cloud is empty.
		if (!point_cloud || point_cloud->get_vertices().empty())
		{
			near_p = conservative_near_plane;
			far_p = conservative_far_plane;
			return;
		}

		Eigen::Matrix4f world_to_camera = camera.get_world_to_camera().matrix();
		Eigen::Matrix4f camera_to_clip = camera.get_camera_to_clip(conservative_near_plane,
			conservative_far_plane);
		std::vector<float> depths;
		depths.reserve(point_cloud->get_vertices().size());
		for (float3 vertex : point_cloud->get_vertices())
		{
			Eigen::Vector3f world_vertex = make_vec3f(vertex);
			Eigen::Vector4f camera_vertex = world_to_camera * world_vertex.homogeneous();
			Eigen::Vector4f clip_vertex = camera_to_clip * camera_vertex;

			// Ignore points that fall outside the conservative camera frustum.
			if (clip_vertex.x() < -clip_vertex.w() || clip_vertex.x() > clip_vertex.w() ||
				clip_vertex.y() < -clip_vertex.w() || clip_vertex.y() > clip_vertex.w() ||
				clip_vertex.z() < -clip_vertex.w() || clip_vertex.z() > clip_vertex.w())
				continue;

			depths.emplace_back(-camera_vertex.z());
		}

		// Use percentiles for a robust estimate of the near/far planes.
		std::sort(depths.begin(), depths.end());

		near_p = depths[(depths.size() * 1) / 100] * 0.75f;
		far_p = depths[(depths.size() * 99) / 100] * 10.0f;
	}

	float plane_to_depth(int plane, int num_depth_planes, float near_plane, float far_plane, bool use_sqrt)
	{
		const float min_disparity = use_sqrt ? std::sqrt(1.0f / far_plane) : 1.0f / far_plane;
		const float max_disparity = use_sqrt ? std::sqrt(1.0f / near_plane) : 1.0f / near_plane;

		float disparity = min_disparity + (max_disparity - min_disparity) * plane / (num_depth_planes - 1.0f);
		float depth = use_sqrt ? 1.0f / (disparity * disparity) : 1.0f / disparity;

		return depth;
	}

	float depth_to_plane(float depth, int num_depth_planes, float near_plane, float far_plane, bool use_sqrt)
	{
		const float min_disparity = use_sqrt ? std::sqrt(1.0f / far_plane) : 1.0f / far_plane;
		const float max_disparity = use_sqrt ? std::sqrt(1.0f / near_plane) : 1.0f / near_plane;

		float disparity = use_sqrt ? 1.0f / std::sqrt(depth) : 1.0f / depth;
		float plane = (disparity - min_disparity) * (num_depth_planes - 1.0f) / (max_disparity - min_disparity);

		return plane;
	}

	template<typename T>
	void resample_volume(CommonParameters params, cv::Size input_size,
		CalibratedImage &input_camera, float input_near, float input_far,
		CalibratedImage &output_camera, float output_near, float output_far,
		std::vector<T> &output_frustum_volume,
		std::function<typename T::value_type(int x, int y, int z)> fetch_func)
	{
		assert(!output_camera.get_image().empty());

		cv::Size output_size = output_camera.get_image().size();
		output_frustum_volume.assign(params.num_depth_planes, T());

		Eigen::Matrix4f output_to_world = output_camera.get_world_to_camera().inverse().matrix();
		Eigen::Matrix3f output_inv_intrinsics = output_camera.get_intrinsics().inverse();

		Eigen::Matrix4f world_to_input = input_camera.get_world_to_camera().matrix();
		Eigen::Matrix4f input_intrinsics = Eigen::Matrix4f::Identity();
		input_intrinsics.block<3, 3>(0, 0) = input_camera.get_intrinsics();

		Eigen::Matrix4f output_to_input = world_to_input * output_to_world;

#ifdef NDEBUG
#pragma omp parallel for schedule(dynamic)
#endif
		for (int plane = params.num_depth_planes - 1; plane >= 0; --plane)
		{
			T     values = T::zeros(output_size);
			float depth = plane_to_depth(plane,
				params.num_depth_planes,
				output_near, output_far,
				params.use_sqrt_disparity);

			for (int y = 0; y < output_size.height; ++y)
				for (int x = 0; x < output_size.width; ++x)
				{
					float fx = output_camera.get_scale_factor() *
						output_camera.get_resolution().x() *
						static_cast<float>(x) / output_size.width;
					float fy = output_camera.get_scale_factor() *
						output_camera.get_resolution().y() *
						static_cast<float>(y) / output_size.height;

					Eigen::Vector3f image_output(fx, fy, 1.0f);
					Eigen::Vector3f position_output = depth * (output_inv_intrinsics * image_output);
					Eigen::Vector4f position_input = output_to_input * position_output.homogeneous();

					if (position_input.z() >= 0.0f)
						continue;

					Eigen::Vector4f image_input = input_intrinsics * position_input;
					image_input /= image_input.z();
					if (image_input.x() < 0 || image_input.x() >= input_size.width ||
						image_input.y() < 0 || image_input.y() >= input_size.height)
						continue;

					float input_depth = std::min(input_far, std::max(input_near, -position_input.z()));
					float input_plane = depth_to_plane(input_depth,
						params.num_depth_planes,
						input_near, input_far,
						params.use_sqrt_disparity);

					// Trilinear fetch with PCF.
					typedef typename T::value_type ValueType;

					float     total_weight = 0.0f;
					ValueType total_value = 0;
					for (int dz = 0; dz <= 1; ++dz)
						for (int dy = 0; dy <= 1; ++dy)
							for (int dx = 0; dx <= 1; ++dx)
							{
								int virtual_plane = int(input_plane) + dz;
								int virtual_y = int(image_input.y()) + dy;
								int virtual_x = int(image_input.x()) + dx;

								int fetch_y = std::min(input_size.height - 1, std::max(0, virtual_y));
								int fetch_x = std::min(input_size.width - 1, std::max(0, virtual_x));
								int fetch_plane = std::min(params.num_depth_planes - 1, std::max(0, virtual_plane));

								float weight = (1.0f - std::abs(virtual_plane - input_plane)) *
									(1.0f - std::abs(virtual_y - image_input.y())) *
									(1.0f - std::abs(virtual_x - image_input.x()));

								// Account for the y-up/down mismatch between our GL coordinates and OpenCV image space.
								int       fetch_yy = input_size.height - 1 - fetch_y;
								ValueType value = fetch_func(fetch_x, fetch_yy, fetch_plane);

								total_weight += weight;
								total_value += weight * value;
							}

					// Account for the y-up/down mismatch between our GL coordinates and OpenCV image space.
					int yy = output_size.height - 1 - y;
					values(yy, x) += total_value / total_weight;
				}
			output_frustum_volume[plane] = values;
		}
	}

	void compute_visibility(CommonParameters params,
		CalibratedImage &ref_camera, float ref_near, float ref_far,
		CalibratedImage::Vector &cameras,
		const std::vector<float> &near_planes,
		const std::vector<float> &far_planes,
		int num_neighbor_views, std::vector<NeighborIndex> &neighbors,
		const std::vector<std::vector<cv::Mat1b>> &consensus_volumes,
		std::vector<std::vector<cv::Mat1b>> &visibility_volumes)
	{
		std::cout << "\rComputing visibility volumes: 000/"
			<< std::setw(3) << std::setfill('0')
			<< num_neighbor_views << std::flush;

		int progress = 0;
		visibility_volumes.assign(num_neighbor_views, std::vector<cv::Mat1b>());
		for (int i = 0; i < num_neighbor_views; i++)
		{
			const int ni = neighbors[i].second;
			CalibratedImage &neighbor_camera = cameras[ni];
			cv::Size         neighbor_size = neighbor_camera.get_image().size();

			std::vector<cv::Mat1b> neighbor_visibility(params.num_depth_planes);
			cv::Mat1b zeros = cv::Mat1b::zeros(neighbor_size);
			cv::Mat1b visibility = 255 * cv::Mat1b::ones(neighbor_size);
			for (int plane = params.num_depth_planes - 1; plane >= 0; --plane)
			{
				neighbor_visibility[plane] = visibility.clone();
				visibility = cv::max(zeros, visibility - consensus_volumes[ni][plane]);
			}

			resample_volume(params, neighbor_size,
				neighbor_camera, near_planes[ni], far_planes[ni],
				ref_camera, ref_near, ref_far,
				visibility_volumes[i],
				[&](int x, int y, int z) {
				return neighbor_visibility[z](y, x);
			});

			{
				progress++;
				std::cout << "\rComputing visibility volumes: "
					<< std::setfill('0') << std::setw(3)
					<< progress << "/"
					<< std::setw(3) << std::setfill('0')
					<< num_neighbor_views << std::flush;
			}
		}
		std::cout << std::endl;
	}

	//
	// Multi-view stereo, consensus volumes and rendering.
	//

	struct MatchingPyramid
	{
		typedef std::vector<cv::Mat1b> SeparatedImage;
		std::vector<SeparatedImage> levels_rgb;
		std::vector<cv::Mat1b> levels_grad_x;
		std::vector<cv::Mat1b> levels_grad_y;

		void build_pyramid(const cv::Mat image, int min_width, int channels)
		{
			SeparatedImage separated(channels);
			cv::split(image, separated);
			cv::Mat1b grey = separated[0] / 3 + separated[1] / 3 + separated[2] / 3;
			do
			{
				levels_rgb.push_back(separated);
				cv::Mat1s grad_x, grad_y;
				cv::Mat1b abs_grad_x, abs_grad_y;

				cv::Sobel(grey, grad_x, CV_16S, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
				cv::convertScaleAbs(grad_x, abs_grad_x);
				levels_grad_x.push_back(abs_grad_x);

				cv::Sobel(grey, grad_y, CV_16S, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);
				cv::convertScaleAbs(grad_y, abs_grad_y);
				levels_grad_y.push_back(abs_grad_y);

				SeparatedImage downscaled(channels);
				for (int i = 0; i < channels; ++i)
					cv::pyrDown(separated[i], downscaled[i]);
				downscaled.swap(separated);
				cv::pyrDown(grey, grey);
			} while (separated[0].cols > min_width);
		}

		MatchingPyramid(const cv::Mat3b &image, int min_width) { build_pyramid(image, min_width, 3); }
		MatchingPyramid(const cv::Mat4b &image, int min_width) { build_pyramid(image, min_width, 4); }
	};

	struct FilterParameters
	{
		// Guided filter parameters (found by experimenting until the depth maps match Fig5)
		int    guided_filter_radius = 4;
		float  guided_filter_eps = 6.5f; // Matches the parameters in [Ma et al. ICCV 2013]

		// Pyarmid aggregation settings
		int    downsampling_cutoff_resolution = 128;
		bool   use_variance_averaging = false;
		int    variance_radius_lowres = 11; // Matches a Gaussian blur with a ~3 px std [Kovesi, DICTA 2010]
		int    variance_radius_highres = 33; // Matches a Gaussian blur with a ~10 px std [Kovesi, DICTA 2010]
	};

	struct FilterPyramid
	{
		std::vector<cv::Mat3b> reference_levels;
		std::vector<cv::Mat1f> variance_lowres_levels;
		std::vector<cv::Mat1f> variance_highres_levels;

		std::vector<cv::Mat1f> cost_levels;
		std::vector<cv::Mat1f> coverage_levels;
		std::vector<cv::Mat1f> division_guards;
		std::vector<cv::Mat1f> invalid_costs;

		FilterPyramid(cv::Mat3b reference_image, float invalid_cost, const FilterParameters &params)
		{
			cv::Size   image_size = reference_image.size();
			cv::Mat1f  zeros = cv::Mat1f::zeros(image_size);
			cv::Mat1f  ones = cv::Mat1f::ones(image_size);

			do
			{
				reference_levels.push_back(reference_image.clone());
				cost_levels.push_back(zeros.clone());
				coverage_levels.push_back(zeros.clone());
				division_guards.push_back(ones.clone());
				invalid_costs.push_back(invalid_cost * ones.clone());

				cv::pyrDown(reference_image, reference_image);
				cv::pyrDown(zeros, zeros);
				cv::pyrDown(ones, ones);
			} while (zeros.cols > params.downsampling_cutoff_resolution);

			if (params.use_variance_averaging)
			{
				for (const cv::Mat3b &reference : reference_levels)
				{
					variance_lowres_levels.emplace_back(compute_variance(reference, params.variance_radius_lowres));
					variance_highres_levels.emplace_back(compute_variance(reference, params.variance_radius_highres));
				}
			}
		}

		void reset()
		{
			cv::Mat1f zeros = cv::Mat1f::zeros(cost_levels[0].size());
			for (int i = 0; i < static_cast<int>(cost_levels.size()); ++i)
			{
				cost_levels[i] = zeros.clone();
				coverage_levels[i] = zeros.clone();
				cv::pyrDown(zeros, zeros);
			}
		}

		void assign(const cv::Mat1f &cost)
		{
			cv::Mat1f level_cost = cost.clone();

			for (int i = 0; i < static_cast<int>(cost_levels.size()); ++i)
			{
				cost_levels[i] = level_cost;
				coverage_levels[i] = cv::Mat1f::ones(level_cost.size());
				cv::pyrDown(level_cost, level_cost);
			}
		}

		void assign(const cv::Mat1f &cost, const cv::Mat1f &coverage)
		{
			cv::Mat1f level_cost = cost.clone();
			cv::Mat1f level_coverage = coverage.clone();

			for (int i = 0; i < static_cast<int>(cost_levels.size()); ++i)
			{
				cost_levels[i] = level_cost;
				coverage_levels[i] = level_coverage;
				cv::pyrDown(level_cost, level_cost);
				cv::pyrDown(level_coverage, level_coverage);
			}
		}

		cv::Mat1f get_final_cost(const FilterParameters &params)
		{
			cv::Mat1f prev_cost;
			for (int i = int(cost_levels.size()) - 1; i >= 0; --i)
			{
				cv::Mat1b guard_mask = coverage_levels[i] <= 0.0f;
				division_guards[i].copyTo(coverage_levels[i], guard_mask);
				invalid_costs[i].copyTo(cost_levels[i], guard_mask);
				cost_levels[i] /= coverage_levels[i];

				if (!prev_cost.empty())
				{
					if (params.use_variance_averaging)
					{
						cost_levels[i] =
							(variance_lowres_levels[i].mul(cost_levels[i]) + variance_highres_levels[i].mul(prev_cost)) /
							(variance_lowres_levels[i] + variance_highres_levels[i]);
					}
					else
					{
						cost_levels[i] = 0.5f * (cost_levels[i] + prev_cost);
					}
				}

				cv::ximgproc::guidedFilter(reference_levels[i], cost_levels[i], cost_levels[i],
					params.guided_filter_radius, params.guided_filter_eps);

				if (i > 0)
					cv::resize(cost_levels[i], prev_cost, cost_levels[i - 1].size());
			}

			return cost_levels[0];
		}
	};

	struct StereoParameters : public CommonParameters, public FilterParameters
	{
		// General parameters
		int    num_neighbor_views = 4;

		// Unary cost (From Hosni et al. ICME 2011)
		float  rgb_weight = 0.9f;
		float  gradient_weight = 0.1f;
		int    rgb_clamp = 7;
		int    gradient_clamp = 2;
	};

	struct StereoPyramid : public FilterPyramid
	{
		float rgb_weight;
		int   rgb_clamp;
		float grad_weight;
		int   grad_clamp;

		StereoPyramid(cv::Mat3b reference_image, const StereoParameters &params)
			: FilterPyramid(reference_image,
				params.gradient_weight * params.gradient_clamp + // invalid_cost, i.e. the largest
				params.rgb_weight      * params.rgb_clamp,       // possible stereo cost.
				params),
			rgb_weight(params.rgb_weight), rgb_clamp(params.rgb_clamp),
			grad_weight(params.gradient_weight), grad_clamp(params.gradient_clamp)
		{
		}

		void match_with_neighbor(const MatchingPyramid &reference,
			const MatchingPyramid &neighbor,
			const cv::Mat1b *visibility)
		{

			cv::Mat1f level_visibility;
			if (visibility)
			{
				level_visibility = *visibility;
				level_visibility /= 255.0f;
			}

			for (int i = 0; i < static_cast<int>(cost_levels.size()); ++i)
			{
				for (int j = 0; j < 3; ++j)
				{
					cv::Mat1f channel_error = cv::min(cv::abs(neighbor.levels_rgb[i][j] -
						reference.levels_rgb[i][j]),
						rgb_clamp);

					cv::Mat1f rgb_cost =
						rgb_weight * channel_error.mul(cv::Mat1f(neighbor.levels_rgb[i][3])) * (1.0f / 3.0f);
					if (visibility)
						rgb_cost = rgb_cost.mul(level_visibility);

					cost_levels[i] += rgb_cost;
				}

				cv::Mat1f grad_error_x = cv::min(cv::abs(neighbor.levels_grad_x[i] -
					reference.levels_grad_x[i]),
					grad_clamp);
				cv::Mat1f grad_error_y = cv::min(cv::abs(neighbor.levels_grad_y[i] -
					reference.levels_grad_y[i]),
					grad_clamp);

				cv::Mat1f grad_cost = 0.5f * grad_weight * (grad_error_x + grad_error_y);
				if (visibility)
					grad_cost = grad_cost.mul(level_visibility);

				cost_levels[i] += grad_cost;

				cv::Mat1f coverage = cv::Mat1f(neighbor.levels_rgb[i][3]);
				if (visibility)
					coverage = coverage.mul(level_visibility);

				coverage_levels[i] += coverage;

				if (visibility)
					cv::pyrDown(level_visibility, level_visibility);
			}
		}

		cv::Mat1f get_final_cost(const StereoParameters &params)
		{
			// This is not a part of the paper, but we can get rid of
			// small floaters by prefiltering the cost volume with a shiftable
			// matching window. [Hedman et al. SIGGRAPH Asia 2017]
#if 1
			for (int i = int(cost_levels.size()) - 1; i >= 0; --i)
			{
				cv::Mat1b guard_mask = coverage_levels[i] <= 0.0f;
				division_guards[i].copyTo(coverage_levels[i], guard_mask);
				invalid_costs[i].copyTo(cost_levels[i], guard_mask);
				cost_levels[i] /= coverage_levels[i];
				coverage_levels[i] = cv::Mat1f::ones(coverage_levels[i].size());

				cv::blur(cost_levels[i], cost_levels[i], cv::Size(3, 3));
				cv::erode(cost_levels[i], cost_levels[i], cv::Mat1f::ones(3, 3));
			}
#endif

			return FilterPyramid::get_final_cost(params);
		}
	};

	cv::Mat1f compute_depth_map(size_t index, CalibratedImage::Vector &cameras,
		const std::vector<float> &near_planes,
		const std::vector<float> &far_planes,
		const std::vector<std::vector<cv::Mat1b>> &consensus_volumes,
		StereoParameters params)
	{
		std::vector<NeighborIndex> neighbors =
			find_neighbors(cameras[index].get_position(), cameras, DiscardExactMatches);

		CalibratedImage &ref_camera = cameras[index];
		ref_camera.load_image(params.max_depth_map_width);

		for (int i = 0; i < params.num_neighbor_views; i++)
		{
			const int ni = neighbors[i].second;
			CalibratedImage &camera = cameras[ni];
			camera.load_image(params.max_depth_map_width);
		}

		std::vector<std::vector<cv::Mat1b>> visibility_volumes;
		const bool use_visibility = !consensus_volumes.empty() && !consensus_volumes[0].empty();
		if (use_visibility)
			compute_visibility(params,
				ref_camera, near_planes[index], far_planes[index],
				cameras, near_planes, far_planes,
				params.num_neighbor_views, neighbors,
				consensus_volumes, visibility_volumes);

		cv::Mat1f depth_map = cv::Mat1f::zeros(ref_camera.get_image().size());
		cv::Mat1f cost_map = cv::Mat1f::ones(ref_camera.get_image().size()) * 1e21f;

		Eigen::Matrix4f world_to_reference = ref_camera.get_world_to_camera().matrix();
		Eigen::Matrix4f reference_intrinsics = Eigen::Matrix4f::Identity();
		reference_intrinsics.block<3, 3>(0, 0) = ref_camera.get_intrinsics();

		cv::Mat3b reference_image = ref_camera.get_image();
		cv::Size  image_size = reference_image.size();

		MatchingPyramid reference_pyramid(reference_image, params.downsampling_cutoff_resolution);

		std::cout << "\rComputing depth: 000/"
			<< std::setw(3) << std::setfill('0') << params.num_depth_planes << std::flush;
		int progress = 0;
#ifdef NDEBUG
#pragma omp parallel for schedule(dynamic)
#endif
		for (int plane = params.num_depth_planes - 1; plane >= 0; --plane)
		{
			float depth = plane_to_depth(plane, params.num_depth_planes,
				near_planes[index], far_planes[index],
				params.use_sqrt_disparity);

			// Construct the planar homography using the technique from Szeliski 2010 (Chapter 2.1.5, eq. 2.70)
			Eigen::Vector4f plane_equation = ref_camera.get_forward().homogeneous();
			plane_equation.w() = -(ref_camera.get_forward().dot(ref_camera.get_position()) + depth);

			Eigen::Matrix4f plane_to_reference = reference_intrinsics * world_to_reference;
			plane_to_reference.row(3) = plane_equation;

			Eigen::Matrix4f reference_to_plane = plane_to_reference.inverse();

			StereoPyramid cost_pyramid(reference_image, params);

#if DEBUG_STEREO
			cv::Mat1f totalvis = 0.0001f * cv::Mat1f::zeros(image_size);
			cv::Mat3f average = cv::Mat3f::zeros(image_size);
#endif

			for (int i = 0; i < params.num_neighbor_views; i++)
			{
				CalibratedImage &neighbor = cameras[neighbors[i].second];
				Eigen::Matrix4f world_to_neighbor = neighbor.get_world_to_camera().matrix();
				Eigen::Matrix4f neighbor_intrinsics = Eigen::Matrix4f::Identity();
				neighbor_intrinsics.block<3, 3>(0, 0) = neighbor.get_intrinsics();

				Eigen::Matrix4f homography_full = neighbor_intrinsics * world_to_neighbor * reference_to_plane;
				Eigen::Matrix3f homography_eigen = homography_full.block<3, 3>(0, 0);
				cv::Mat homography_cv;
				cv::eigen2cv(homography_eigen, homography_cv);

				cv::Mat4b flipped_neighbor;
				cv::flip(add_alpha_channel(neighbor.get_image()), flipped_neighbor, 0);

				cv::Mat4b warped_neighbor;
				cv::warpPerspective(flipped_neighbor, warped_neighbor, homography_cv, image_size,
					cv::INTER_LINEAR | cv::WARP_INVERSE_MAP);
				cv::flip(warped_neighbor, warped_neighbor, 0);

				MatchingPyramid neighbor_pyramid(warped_neighbor, params.downsampling_cutoff_resolution);
				cost_pyramid.match_with_neighbor(reference_pyramid, neighbor_pyramid,
					use_visibility ? &visibility_volumes[i][plane] : 0);

#if DEBUG_STEREO
				std::vector<cv::Mat1b> split_neighbor(4);
				cv::split(warped_neighbor, split_neighbor);

				cv::Mat1f n_r = split_neighbor[0];
				cv::Mat1f n_g = split_neighbor[1];
				cv::Mat1f n_b = split_neighbor[2];
				cv::Mat1f vis = use_visibility ? visibility_volumes[i][plane] : 255 * cv::Mat1b::ones(image_size);
				vis /= 255.0f;

				std::vector<cv::Mat1f> splitf =
				{
				n_r.mul(vis), n_g.mul(vis), n_b.mul(vis)
				};

				cv::Mat3f weighted;
				cv::merge(splitf, weighted);

				totalvis += vis;
				average += weighted;
#endif
			}

			cv::Mat1f plane_depth = depth * cv::Mat1f::ones(depth_map.size());
			cv::Mat1f plane_cost = cost_pyramid.get_final_cost(params);

#if DEBUG_STEREO
			std::vector<cv::Mat1f> split_avg(3);
			cv::split(average, split_avg);
			split_avg[0] = split_avg[0] / totalvis;
			split_avg[1] = split_avg[1] / totalvis;
			split_avg[2] = split_avg[2] / totalvis;
			cv::merge(split_avg, average);
			average = average;

			std::stringstream dump_stream;
			dump_stream << std::setfill('0') << std::setw(3) << plane;
			if (!directory_exists("imgs"))
				make_directory("imgs");
			if (!directory_exists("costs"))
				make_directory("costs");
			{
				cv::Mat3b avg_8bit = average;
				cv::imwrite("imgs/" + dump_stream.str() + ".jpg", avg_8bit);

				const float max_cost = params.rgb_weight * params.rgb_clamp +
					params.gradient_weight * params.gradient_clamp;
				cv::Mat1f cost_float = plane_cost;
				cost_float *= 255.0f / max_cost;
				cv::Mat1b cost_8bit = cost_float;
				cv::imwrite("costs/" + dump_stream.str() + ".png", cost_8bit);
			}
#endif

#pragma omp critical
			{
				cv::Mat1b update_mask = plane_cost < cost_map;
				plane_depth.copyTo(depth_map, update_mask);
				plane_cost.copyTo(cost_map, update_mask);

				progress++;
				std::cout << "\rComputing depth: "
					<< std::setfill('0') << std::setw(3)
					<< progress << "/"
					<< std::setw(3) << std::setfill('0')
					<< params.num_depth_planes << std::flush;
			}
		}
		std::cout << std::endl;

#if DEBUG_STEREO
		const float min_disp = 1.0f / std::sqrt(far_planes[index]);
		const float max_disp = 1.0f / std::sqrt(near_planes[index]);
		cv::Mat1b dump_map = cv::min(max_disp, cv::max(min_disp, 1.0f / depth_map)) * 255.0f / max_disp;
		cv::imwrite("depth.png", dump_map);
		cv::imwrite("image.jpg", reference_image);
#endif

		// Clear the images from memory.
		for (int i = 0; i < params.num_neighbor_views; i++)
		{
			CalibratedImage &camera = cameras[neighbors[i].second];
			camera.clear_image();
		}
		ref_camera.clear_image();


		return depth_map;
	}

	struct ConsensusVolumeParameters : public CommonParameters, public FilterParameters
	{
		int   num_neighbor_views = 9;
		int   tolerance_depth = 1;
		int   tolerance_xy = 1;
		float truncation_threshold = 0.5f;
	};

	void build_consesus_volume(size_t index,
		CalibratedImage::Vector &cameras,
		const std::vector<cv::Mat1f> &depth_maps,
		const std::vector<float> &near_planes,
		const std::vector<float> &far_planes,
		ConsensusVolumeParameters params,
		std::vector<cv::Mat1b> &consensus_volume)
	{
		params.use_variance_averaging = true;
		params.num_neighbor_views = std::min(params.num_neighbor_views,
			static_cast<int>(cameras.size()) - 1);
		std::vector<NeighborIndex> neighbors =
			find_neighbors(cameras[index].get_position(), cameras, DiscardExactMatches);

		CalibratedImage &ref_camera = cameras[index];
		ref_camera.load_image(params.max_depth_map_width);

		for (int i = 0; i < params.num_neighbor_views; i++)
		{
			CalibratedImage &camera = cameras[neighbors[i].second];
			camera.load_image(params.max_depth_map_width);
		}

		cv::Mat3b reference_image = ref_camera.get_image();
		cv::Mat1f reference_depth = depth_maps[index];
		cv::Size  image_size = reference_image.size();
		consensus_volume.resize(params.num_depth_planes);

		std::vector<cv::Mat2f> value_confidence_volume;
		for (int plane = 0; plane < params.num_depth_planes; ++plane)
			value_confidence_volume.emplace_back(cv::Mat2f::zeros(image_size));

#ifdef NDEBUG
#pragma omp parallel for schedule(dynamic)
#endif
		for (int plane = params.num_depth_planes - 1; plane >= 0; --plane)
			for (int y = 0; y < image_size.height; ++y)
				for (int x = 0; x < image_size.width; ++x)
				{
					float depth = plane_to_depth(plane, params.num_depth_planes,
						near_planes[index], far_planes[index],
						params.use_sqrt_disparity);

					float value = reference_depth(y, x) == depth ? 1.0f : 0.0f;
					float confidence = reference_depth(y, x) >= depth ? 1.0f : 0.0f;
					value_confidence_volume[plane](y, x) = cv::Vec2f(value, confidence);
				}

		std::cout << "\rBuilding consensus volume: 000/"
			<< std::setfill('0') << std::setw(3)
			<< params.num_neighbor_views << std::flush;

		std::vector<cv::Mat2f> temp_volume(params.num_depth_planes);
		for (int i = 0; i < params.num_neighbor_views; i++)
		{
			const int ni = neighbors[i].second;
			CalibratedImage &neighbor = cameras[ni];
			const cv::Mat1f &neighbor_depths = depth_maps[ni];

			resample_volume(params, neighbor_depths.size(),
				neighbor, near_planes[ni], far_planes[ni],
				ref_camera, near_planes[index], far_planes[index],
				temp_volume,
				[&](int x, int y, int z) {
				float depth = plane_to_depth(z, params.num_depth_planes,
					near_planes[ni], far_planes[ni],
					params.use_sqrt_disparity);
				float confidence = neighbor_depths(y, x) >= depth ? 1.0f : 0.0f;
				float value = neighbor_depths(y, x) == depth ? 1.0f : 0.0f;

				return cv::Vec2f(value, confidence);
			});

#ifdef NDEBUG
#pragma omp parallel for
#endif
			for (int plane = params.num_depth_planes - 1; plane >= 0; --plane)
			{
				cv::Mat2f temp_plane = temp_volume[plane].clone();
				for (int delta = -params.tolerance_depth; delta <= params.tolerance_depth; ++delta)
				{
					int other_plane = plane + delta;
					if (other_plane < 0 || other_plane == plane || other_plane >= params.num_depth_planes)
						continue;

					temp_plane = cv::max(temp_plane, temp_volume[other_plane]);
				}

				if (params.tolerance_xy > 0)
					cv::dilate(temp_plane, temp_plane, cv::Mat(), cv::Point(-1, -1), params.tolerance_xy);

				value_confidence_volume[plane] += temp_plane;
			}

			std::cout << "\rBuilding consensus volume: "
				<< std::setfill('0') << std::setw(3)
				<< i + 1 << "/"
				<< std::setfill('0') << std::setw(3)
				<< params.num_neighbor_views << std::flush;
		}
		std::cout << std::endl;

#ifdef NDEBUG
#pragma omp parallel for schedule(dynamic)
#endif
		for (int plane = params.num_depth_planes - 1; plane >= 0; --plane)
		{
			cv::Mat2f& values_confidences = value_confidence_volume[plane];

			// Subract one vote from the total. This how the paper compensates
			// for the tolerance interval in depth (Section 5.2).
			values_confidences = cv::max(0.0f, values_confidences - 1.0f);

			cv::Mat1f consensus_plane = cv::Mat1f::zeros(image_size);
			for (int y = 0; y < image_size.height; ++y)
				for (int x = 0; x < image_size.width; ++x)
				{
					float consensus = values_confidences(y, x)[0];

					const float min_confidence = params.num_neighbor_views * 0.5f;
					if (values_confidences(y, x)[1] < min_confidence)
						consensus *= values_confidences(y, x)[1] / min_confidence;

					if (values_confidences(y, x)[1] > 0.0f)
						consensus /= values_confidences(y, x)[1];

					consensus_plane(y, x) = consensus;
				}

			FilterPyramid pyramid(reference_image, 0.0f, params);
			pyramid.assign(consensus_plane);
			consensus_plane = pyramid.get_final_cost(params);

			// Convert to 8-bit, but make sure that non-zero entries do not get truncated to zero.
			consensus_plane *= 255.0f;
			cv::Mat1b non_zero = consensus_plane > params.truncation_threshold;
			cv::Mat1f clamped = cv::Mat1f::ones(image_size);
			clamped = cv::max(clamped, consensus_plane);
			clamped.copyTo(consensus_plane, non_zero);

			consensus_volume[plane] = consensus_plane;
		}

		// Clear the images from memory.
		for (int i = 0; i < params.num_neighbor_views; i++)
		{
			CalibratedImage &camera = cameras[neighbors[i].second];
			camera.clear_image();
		}
		ref_camera.clear_image();
	}

	cv::Mat1f average_depth_map(const std::vector<cv::Mat1b> &consensus_volume,
		float near_p, float far_p, bool use_sqrt)
	{
		const int num_depth_planes = int(consensus_volume.size());
		cv::Size  image_size = consensus_volume[0].size();
		cv::Mat1f total_disparity = cv::Mat1f::zeros(image_size);
		cv::Mat1f total_confidence = 0.0000001f * cv::Mat1f::ones(image_size);

		cv::Mat1b zeros_byte = cv::Mat1b::zeros(image_size);
		cv::Mat1f zeros_float = cv::Mat1f::zeros(image_size);
		cv::Mat1b visibility = 255 * cv::Mat1b::ones(image_size);
		for (int plane = num_depth_planes - 1; plane >= 0; --plane)
		{
			cv::Mat1b visibility_mask = visibility == 0;
			cv::Mat1f consensus_float = consensus_volume[plane];
			cv::Mat1f disparity = plane * cv::Mat1f::ones(image_size);
			zeros_float.copyTo(disparity, visibility_mask);
			zeros_float.copyTo(consensus_float, visibility_mask);

			total_disparity += disparity.mul(consensus_float);
			total_confidence += consensus_float;

			visibility = cv::max(zeros_byte, visibility - consensus_volume[plane]);
		}
		total_disparity /= total_confidence;

		cv::Mat1f depth_map(image_size);
		for (int y = 0; y < image_size.height; ++y)
			for (int x = 0; x < image_size.width; ++x)
				depth_map(y, x) = plane_to_depth(total_disparity(y, x), num_depth_planes, near_p, far_p, use_sqrt);

		return depth_map;
	}

	cv::Mat1f median_depth_map(const std::vector<cv::Mat1b> &consensus_volume,
		float near_p, float far_p, bool use_sqrt)
	{
		const int num_depth_planes = int(consensus_volume.size());
		cv::Size  image_size = consensus_volume[0].size();

		cv::Mat1b zeros_byte = cv::Mat1b::zeros(image_size);
		cv::Mat1f zeros_float = cv::Mat1f::zeros(image_size);
		cv::Mat1b visibility = 255 * cv::Mat1b::ones(image_size);
		cv::Mat1f total_confidence = cv::Mat1f::zeros(image_size);
		std::vector<cv::Mat1f> confidence_volume(num_depth_planes);
		for (int plane = num_depth_planes - 1; plane >= 0; --plane)
		{
			cv::Mat1b visibility_mask = visibility == 0;
			cv::Mat1f consensus_float = consensus_volume[plane];
			zeros_float.copyTo(consensus_float, visibility_mask);

			confidence_volume[plane] = consensus_float;
			total_confidence += consensus_float;

			visibility = cv::max(zeros_byte, visibility - consensus_volume[plane]);
		}

		cv::Mat1f depth_map(image_size);
		for (int y = 0; y < image_size.height; ++y)
			for (int x = 0; x < image_size.width; ++x)
			{
				float target_confidence = total_confidence(y, x) * 0.5f;
				float cumulative_confidence = 0.0f;
				float median_plane = 0;
				for (int plane = 0; plane < num_depth_planes; ++plane)
				{
					cumulative_confidence += confidence_volume[plane](y, x);
					if (cumulative_confidence >= target_confidence)
					{
						median_plane = plane;
						break;
					}
				}

				depth_map(y, x) = plane_to_depth(median_plane, num_depth_planes, near_p, far_p, use_sqrt);
			}

		return depth_map;
	}

	struct RenderParameters : public CommonParameters
	{
		int num_neighbor_views = 4;
	};

	cv::Mat3b render_ibr(Eigen::Vector3f position,
		Eigen::Matrix4f world_to_camera,
		float fov, Eigen::Vector2i resolution,
		const std::vector<float> &near_planes,
		const std::vector<float> &far_planes,
		const std::vector<std::vector<cv::Mat1b>> &consensus_volumes,
		CalibratedImage::Vector &cameras, RenderParameters params)
	{

		//SIBR_LOG << "Position: " << position << " World to camera: " << world_to_camera << std::endl;
		std::vector<NeighborIndex> neighbors = find_neighbors(position, cameras, ReturnAllCameras);
		for (int i = 0; i < params.num_neighbor_views; i++)
		{
			CalibratedImage &camera = cameras[neighbors[i].second];
			camera.load_image(params.max_depth_map_width);
		}

		float average_baseline = 0.0f;
		for (int i = 0; i < static_cast<int>(cameras.size()); i++)
		{
			std::vector<NeighborIndex> nearest =
				find_neighbors(cameras[i].get_position(), cameras, DiscardExactMatches);
			average_baseline += nearest[0].first;
		}
		average_baseline /= cameras.size();

		std::vector<cv::Mat1f> weight_images(params.num_neighbor_views);
#ifdef NDEBUG
#pragma omp parallel for schedule(dynamic)
#endif
		for (int i = 0; i < params.num_neighbor_views; i++)
		{
			CalibratedImage &camera = cameras[neighbors[i].second];
			Eigen::Matrix3f neighbor_inv_intrinsics = camera.get_intrinsics().inverse();

			cv::Mat1f weights = cv::Mat1f::zeros(camera.get_image().size());
			for (int y = 0; y < weights.rows; ++y)
				for (int x = 0; x < weights.cols; ++x)
				{
					int yy = weights.rows - 1 - y;
					float fx = camera.get_scale_factor() * camera.get_resolution().x() * static_cast<float>(x) / weights.cols;
					float fy = camera.get_scale_factor() * camera.get_resolution().y() * static_cast<float>(yy) / weights.rows;

					Eigen::Vector3f neighbor_image(fx, fy, 1.0f);
					Eigen::Vector3f ray_neighbor = (neighbor_inv_intrinsics * neighbor_image).normalized();

					Eigen::Vector3f delta_world = position - camera.get_position();
					Eigen::Vector3f delta_ray = ray_neighbor.dot(delta_world) * ray_neighbor;
					Eigen::Vector3f delta_perp = delta_world - delta_ray;

					float distance_squared_to_line = delta_perp.squaredNorm();
					weights(y, x) = std::exp(-distance_squared_to_line / (average_baseline * average_baseline));
				}
			weight_images[i] = weights;
		}

		// Create a fake CalibratedImage, so we can re-use
		// the consensus volume resampling functions.


		//SIBR_LOG << "Position: " << position << " World to camera: " << world_to_camera << std::endl;
		Eigen::Matrix3f orientation = world_to_camera.block<3, 3>(0, 0).transpose();

		//SIBR_LOG << "Position: " << position << " World to camera: " << world_to_camera << std::endl;

		float focal = resolution.y() * 0.5f / std::tan(fov * 0.5f * M_PI / 180.0f);
		float dx = resolution.x() / 2.0f;
		float dy = resolution.y() / 2.0f;


		//SIBR_LOG << "Position: " << position << " World to camera: " << world_to_camera << std::endl;
		CalibratedImage dummy_camera(focal, focal, dx, dy,
			Eigen::Vector3f(0.0f, 0.0f, 0.0f),
			orientation, position, resolution,
			Texture::Ptr(), "dummy_image.jpg");
		dummy_camera.get_image() = cv::Mat3b::zeros(resolution.y(), resolution.x());

		float avg_near = 0.0f;
		float avg_far = 0.0f;
		for (int i = 0; i < params.num_neighbor_views; ++i)
		{
			int ni = neighbors[i].second;
			avg_near += near_planes[ni];
			avg_far += far_planes[ni];
		}
		avg_near /= params.num_neighbor_views;
		avg_far /= params.num_neighbor_views;

		std::vector<std::vector<cv::Mat1b>> neighbor_visibilities;
		if (!consensus_volumes.empty())
			compute_visibility(params,
				dummy_camera, avg_near, avg_far,
				cameras, near_planes, far_planes,
				params.num_neighbor_views, neighbors,
				consensus_volumes, neighbor_visibilities);

		std::cout << "\rResampling consensus volumes: 000/"
			<< std::setw(3) << std::setfill('0')
			<< params.num_neighbor_views << std::flush;

		int progress = 0;
		std::vector<std::vector<cv::Mat1b>> neighbor_consensus(params.num_neighbor_views);
		for (int i = 0; i < params.num_neighbor_views; i++)
		{
			const int ni = neighbors[i].second;
			CalibratedImage &neighbor_camera = cameras[ni];
			cv::Size         neighbor_size = neighbor_camera.get_image().size();

			resample_volume(params, neighbor_size,
				neighbor_camera, near_planes[ni], far_planes[ni],
				dummy_camera, avg_near, avg_far,
				neighbor_consensus[i],
				[&](int x, int y, int z) {
				return consensus_volumes[ni][z](y, x);
			});

			{
				progress++;
				std::cout << "\rResampling consensus volumes: "
					<< std::setfill('0') << std::setw(3)
					<< progress << "/"
					<< std::setw(3) << std::setfill('0')
					<< params.num_neighbor_views << std::flush;
			}
		}
		std::cout << std::endl;

		cv::Size image_size(resolution.x(), resolution.y());
		Eigen::Matrix4f world_to_reference = dummy_camera.get_world_to_camera().matrix();
		Eigen::Matrix4f reference_intrinsics = Eigen::Matrix4f::Identity();
		reference_intrinsics.block<3, 3>(0, 0) = dummy_camera.get_intrinsics();

		std::cout << "\rRendering image: 000/"
			<< std::setw(3) << std::setfill('0')
			<< params.num_depth_planes << std::flush;

		progress = 0;
		cv::Mat1f visibility = 255.0f    * cv::Mat1f::ones(image_size);
		cv::Mat1f image_weight = 0.000001f * cv::Mat1f::ones(image_size);
		cv::Mat3f image = cv::Mat3f::zeros(image_size);
		for (int plane = params.num_depth_planes - 1; plane >= 0; --plane)
		{
			float depth = plane_to_depth(plane, params.num_depth_planes,
				avg_near, avg_far,
				params.use_sqrt_disparity);

			// Construct the planar homography using the technique from Szeliski 2010 (Chapter 2.1.5, eq. 2.70)
			Eigen::Vector4f plane_equation = dummy_camera.get_forward().homogeneous();
			plane_equation.w() = -(dummy_camera.get_forward().dot(dummy_camera.get_position()) + depth);

			Eigen::Matrix4f plane_to_reference = reference_intrinsics * world_to_reference;
			plane_to_reference.row(3) = plane_equation;

			Eigen::Matrix4f reference_to_plane = plane_to_reference.inverse();

			cv::Mat1f total_weight = 0.000001f * cv::Mat1f::ones(image_size);
			cv::Mat1f total_consensus = cv::Mat1f::zeros(image_size);
			cv::Mat3f total_color = cv::Mat3f::zeros(image_size);

			for (int i = 0; i < params.num_neighbor_views; i++)
			{
				const int ni = neighbors[i].second;
				CalibratedImage &neighbor = cameras[ni];
				Eigen::Matrix4f world_to_neighbor = neighbor.get_world_to_camera().matrix();
				Eigen::Matrix4f neighbor_intrinsics = Eigen::Matrix4f::Identity();
				neighbor_intrinsics.block<3, 3>(0, 0) = neighbor.get_intrinsics();

				Eigen::Matrix4f homography_full = neighbor_intrinsics *
					world_to_neighbor *
					reference_to_plane;
				Eigen::Matrix3f homography_eigen = homography_full.block<3, 3>(0, 0);
				cv::Mat homography_cv;
				cv::eigen2cv(homography_eigen, homography_cv);

				cv::Mat4b flipped_neighbor;
				cv::flip(add_alpha_channel(neighbor.get_image()), flipped_neighbor, 0);

				cv::Mat4b warped_neighbor;
				cv::warpPerspective(flipped_neighbor, warped_neighbor,
					homography_cv, image_size,
					cv::INTER_LINEAR | cv::WARP_INVERSE_MAP);
				cv::flip(warped_neighbor, warped_neighbor, 0);

				cv::Mat1f flipped_weights;
				cv::flip(weight_images[i], flipped_weights, 0);
				cv::Mat1f warped_weights;
				cv::warpPerspective(flipped_weights, warped_weights,
					homography_cv, image_size,
					cv::INTER_LINEAR | cv::WARP_INVERSE_MAP);
				cv::flip(warped_weights, warped_weights, 0);

				std::vector<cv::Mat1b> split(4);
				cv::split(warped_neighbor, split);

				cv::Mat1f r = split[0];
				cv::Mat1f g = split[1];
				cv::Mat1f b = split[2];
				cv::Mat1f w = cv::min(split[3], neighbor_visibilities[i][plane]);
				w = w.mul(warped_weights);

				r = w.mul(r);
				g = w.mul(g);
				b = w.mul(b);

				cv::Mat3f rgb;
				std::vector<cv::Mat1f> split_rgb = { r, g, b };
				cv::merge(split_rgb, rgb);
				total_color += rgb;

				cv::Mat1f consensus = neighbor_consensus[i][plane];
				total_consensus += w.mul(consensus);

				total_weight += w;
			}

			total_consensus /= total_weight;
			cv::Mat1f weight = cv::min(visibility, total_consensus);

			std::vector<cv::Mat1f> split(3);
			cv::split(total_color, split);
			split[0] = weight.mul(split[0]) / total_weight;
			split[1] = weight.mul(split[1]) / total_weight;
			split[2] = weight.mul(split[2]) / total_weight;
			cv::merge(split, total_color);

			image += total_color;
			image_weight += weight;

			cv::Mat1f zeros = cv::Mat1f::zeros(image_size);
			visibility = cv::max(zeros, visibility - total_consensus);

			std::cout << "\rRendering image: "
				<< std::setw(3) << std::setfill('0')
				<< params.num_depth_planes - plane << "/"
				<< std::setw(3) << std::setfill('0')
				<< params.num_depth_planes << std::flush;
		}
		std::cout << std::endl;

		std::vector<cv::Mat1f> split(3);
		cv::split(image, split);
		split[0] /= image_weight;
		split[1] /= image_weight;
		split[2] /= image_weight;
		cv::merge(split, image);

		// Clear the images from memory.
		for (int i = 0; i < params.num_neighbor_views; i++)
		{
			CalibratedImage &camera = cameras[neighbors[i].second];
			camera.clear_image();
		}

		return cv::Mat3b(image);
	}


	//
	// Shaders
	//

	fribr::Shader* make_mesh_shader()
	{
		fribr::Shader* shader = new fribr::Shader();
		shader->vertex_shader(
			GLSL(330,
				layout(location = 0) in vec3 vertex_position;
		layout(location = 1) in vec3 vertex_normal;
		layout(location = 2) in vec2 vertex_texcoord;
		layout(location = 3) in vec3 vertex_color;

		uniform mat4 camera_to_clip;
		uniform mat4 world_to_camera;

		out vec2 texcoord;
		out vec3 world_position;
		out vec3 camera_position;
		out vec4 clip_position;

		void main()
		{
			texcoord = vertex_texcoord;
			world_position = vertex_position;
			camera_position = (world_to_camera * vec4(world_position, 1.0)).xyz;
			clip_position = camera_to_clip * vec4(camera_position, 1.0);
			gl_Position = clip_position;
		}
		));

		shader->fragment_shader(
			GLSL(330,
				in  vec2 texcoord;
		in  vec3 world_position;
		in  vec3 camera_position;
		in  vec4 clip_position;
		out vec4 frag_color;

		uniform sampler2D diffuse_sampler;

		void main()
		{
			vec3 pixel_color = texture2D(diffuse_sampler, texcoord).rgb;
			frag_color = vec4(pixel_color, 1.0);
		}
		));
		shader->link();

		return shader;
	}

	fribr::Shader* make_volume_shader()
	{
		fribr::Shader* shader = new fribr::Shader();
		shader->vertex_shader(
			GLSL(330,
				layout(location = 0) in vec2 vertex_position;
		out vec2 texcoord;

		void main()
		{
			texcoord = (vertex_position + vec2(1.0, 1.0)) * 0.5;
			gl_Position = vec4(vertex_position, 0.0, 1.0);
		}
		));

		shader->fragment_shader(
			GLSL(330,
				in  vec2 texcoord;
		out vec4 frag_color;

		uniform sampler2D diffuse_sampler;
		uniform sampler2D consensus_sampler;

		void main()
		{
			vec3  pixel_color = texture2D(diffuse_sampler, texcoord).rgb;
			float pixel_alpha = texture2D(consensus_sampler, texcoord).r;
			frag_color = vec4(pixel_color, pixel_alpha);
		}
		));
		shader->link();

		return shader;
	}

	fribr::Shader* make_normalize_shader()
	{
		fribr::Shader* shader = new fribr::Shader();
		shader->vertex_shader(
			GLSL(330,
				layout(location = 0) in vec2 vertex_position;
		out vec2 texcoord;

		void main()
		{
			texcoord = (vertex_position + vec2(1.0, 1.0)) * 0.5;
			gl_Position = vec4(vertex_position, 0.0, 1.0);
		}
		));

		shader->fragment_shader(
			GLSL(330,
				in  vec2 texcoord;
		out vec4 frag_color;

		uniform sampler2D raw_sampler;

		void main()
		{
			vec4 raw_color = texture2D(raw_sampler, texcoord);
			float normalizer = 1.0f;
			//if (raw_color.a > 0.01f && raw_color.a < 1.0f)
			//	 normalizer = 1.0f / raw_color.a;
			frag_color = vec4(normalizer * raw_color.rgb, 1.0f);
		}
		));
		shader->link();

		return shader;
	}

}


int main(int argc, char* argv[])
{

	CommandLineArgs::parseMainArgs(argc, argv);
	Soft3DIBRAppArgs mainArgs;

	// rendering size
	uint rendering_width = mainArgs.rendering_size.get()[0];
	uint rendering_height = mainArgs.rendering_size.get()[1];

	std::string find_image = mainArgs.dataset_path.get() + "/colmap/stereo/images/00000000.jpg";
	cv::Mat test_image;
	if (fileExists(find_image))
		test_image = fribr::load_image(find_image, int(8192), int(cv::IMREAD_COLOR));
	if (test_image.size().width != 0) {
		rendering_width = test_image.size().width;
		rendering_height = test_image.size().height;
		std::cout << "Using " << rendering_width << "x" << rendering_height << " resolution" << std::endl;
	}

	// Setup window
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
	{
		std::cerr << "Could not initialize glfw." << std::endl;
		return 1;
	}
	
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_SAMPLES, 4);

	// check rendering size
	rendering_width = (rendering_width <= 0) ? 1280 : rendering_width;
	rendering_height = (rendering_height <= 0) ? 720 : rendering_height;
	Vector2u usedResolution(rendering_width, rendering_height);
	
	int screen_w = usedResolution.x(), screen_h = usedResolution.y();
	
	glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
	GLFWwindow* window = glfwCreateWindow(screen_w, screen_h, "Soft3D IBR", NULL, NULL);

	glfwMakeContextCurrent(window);
	glfwSwapInterval(0);

	glewExperimental = GL_TRUE;
	GLenum glew_error = glewInit();
	
	if (GLEW_OK != glew_error)
	{
		std::cerr << "glew init failed: " << glewGetErrorString(glew_error) << std::endl;
		return 1;
	}

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);


	// Debounce values for keys.
	bool home_down = false;
	bool left_down = false;
	bool right_down = false;

	//get output_folder arg
	std::string output_folder = "";
	std::string scene_path = mainArgs.dataset_path.get() + "/colmap/dataset.db";

	if (mainArgs.outPath.get() != "pathOutput") {
		std::cout << " output folder = " << mainArgs.outPath.get() << std::endl;
		output_folder = mainArgs.outPath.get();
		if (!directory_exists(output_folder)) {
			make_directory(output_folder);
			std::cout << "output folder " << output_folder << " created " << std::endl;
		}
	}
	else {
		output_folder = mainArgs.dataset_path.get() + "/pathOutput/";
		if (!directory_exists(output_folder))
			make_directory(output_folder);
		output_folder += "/soft3d/";
		if (!directory_exists(output_folder))
			make_directory(output_folder);
		std::cout << "output folder = " << output_folder << std::endl;
	}

	if (!directory_exists(output_folder)) {
		make_directory(output_folder);
		std::cout << " output folder " << output_folder << " created " << std::endl;
	}
	
	
	float near_p = 1.0f;
	float far_p = 10000.0f;
	float fov = 40.0f;

	
	float customFOV = (mainArgs.fov_arg > 0 ? mainArgs.fov_arg : 0.698132f);
	fov = customFOV*180.0 / M_PI;
	

	float speed = 5.0f;

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	static const float FIRST_PERSON_SENSITIVITY = 0.001f;
	static const float DRAG_SENSITIVITY = 0.01f;
	
	CalibratedImage::Vector cameras;
	PointCloudRenderer::Ptr point_cloud;

	// Stereo data
	std::vector<float> near_planes;
	std::vector<float> far_planes;
	std::vector<cv::Mat1f> depth_maps;
	
	// Consensus volume data
	std::vector<std::vector<cv::Mat1b>> consensus_volumes;

	// Shaders
	Shader::Ptr mesh_shader(make_mesh_shader());
	Shader::Ptr volume_shader(make_volume_shader());
	Shader::Ptr normalize_shader(make_normalize_shader());

	// Video dumping data
	std::vector<Eigen::Matrix4f> video_world_to_camera;

	int   selected_camera = 0;
	bool  compute_single_depth_map = false;
	bool  computing_depth = false;
	bool  depth_from_volume = false;
	bool  show_mesh = false;
	bool  show_sfm = true;
	bool  render_image = false;

	std::string outputVideoPath;
	int sampling_rate = mainArgs.sampling_rate;

	if (sampling_rate <= 0) {
		SIBR_ERR << "Invalid sampling rate provided!!" << std::endl;
		return 0;
	}
	


	float circle_radius = 2.5f;
	float spinrot_height = 1.0f;
	int startFrame = 0;
	enum RenderPath {
		Interpolate = 0,
		Circle = 1,
		SpinRot = 2,
		FromFile = 3
	};
	int   render_path = static_cast<int>(Interpolate);
	int   current_video_frame = 0;
	bool  dumping_video = false;

	bool  build_single_volume = false;
	bool  building_volumes = false;
	bool  show_volume = false;
	int   max_volume_depth = 0;
	int   cached_camera = -1;

	float frustum_scale = 0.2f;
	float camera_point_size = 1.0f;
	float point_size = 5.0f;
	bool  drag_navigation = false;

	StereoParameters          stereo_params;
	ConsensusVolumeParameters consensus_params;
	std::string mypath;

	auto load_scene = [&](const std::string &path)
	{
#define INRIA_WIN 1
#ifdef INRIA_WIN
		// SR: allow loading a bundler+pmvs files.
		if (path.substr(path.size() - 3, 3) == "out") {
			std::cout << "Using bundle file." << std::endl;
			cameras = Bundler::load_calibrated_images(path);
///			point_cloud.reset(new PointCloudRenderer(Bundler::load_point_cloud(path)));
			point_cloud.reset(new PointCloudRenderer(COLMAP::load_point_cloud(path)));
		}
		else {
			cameras = COLMAP::load_calibrated_images(path);
			point_cloud.reset(new PointCloudRenderer(COLMAP::load_point_cloud(path)));
		}
#else
		cameras = COLMAP::load_calibrated_images(path);
		point_cloud.reset(new PointCloudRenderer(COLMAP::load_point_cloud(path)));
#endif

		near_planes.clear();
		far_planes.clear();

		for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
		{
			float near_p, far_p;
			estimate_near_and_far(cameras[i], point_cloud->get_point_cloud(), near_p, far_p);

			//
			near_planes.push_back(near_p);
			far_planes.push_back(far_p);
		}

		depth_maps.clear();
		depth_maps.resize(cameras.size());
		

		consensus_volumes.clear();
		consensus_volumes.resize(cameras.size());
		
		const std::string depth_folder =
			scene_directory_exists(mainArgs.dataset_path, "final_depth") ? "final_depth" : "initial_depth";
		const std::string consensus_folder =
			scene_directory_exists(mainArgs.dataset_path, "final_consensus") ? "final_consensus" : "initial_consensus";

		std::cout << "Loading depth from folder: " << depth_folder << std::endl;
		depth_maps = load_depth_maps(cameras, depth_folder, mainArgs.dataset_path);

		std::cout << "Loading consensus from folder: " << consensus_folder << std::endl;
		load_consensus_volumes(cameras, consensus_folder, consensus_params.num_depth_planes, consensus_volumes, mainArgs.dataset_path);

		for (int i = 0; i < static_cast<int>(depth_maps.size()); ++i)
		{
			if (depth_maps[i].empty())
				continue;

			//depth_meshes[i] = create_mesh(cameras[i], depth_maps[i]);

			cameras[i].load_image(9999);
			/*depth_textures[i] = Texture::Ptr(
				new Texture(cameras[i].get_image(),
					Texture::Descriptor(GL_CLAMP, GL_LINEAR_MIPMAP_LINEAR, Texture::GENERATE_MIPMAPS))
			);*/
			cameras[i].clear_image();
		}
	};

	bool batch_mode = false;
	bool batch_render = false;
	if (mainArgs.batch_reconstruct || mainArgs.batch_render)
	{
		batch_mode = true;
		batch_render = mainArgs.batch_render;
	}
	

	startFrame = mainArgs.start_frame;

	{
		try
		{
			load_scene(scene_path);
			if (batch_render)
			{
				current_video_frame = 0;
				dumping_video = true;

				// sibr version
				CalibratedImage::Vector path_cameras;
//				mypath = mainArgs.dataset_path.get() + "/cameras/" + mainArgs.render_path.get();
				if (mainArgs.pathFile.get() != "") 
					mypath = mainArgs.pathFile.get();
				
				if (sibr::fileExists(mypath)) {
					std::cerr << "Reading " << mypath << std::endl;
					path_cameras = Bundler::load_calibrated_images(mypath, true);
					outputVideoPath = output_folder + "/" + strip_extension(get_filename(mypath)) + ".mp4";
					interpolate_camera_path(video_world_to_camera, path_cameras, sampling_rate, startFrame, mainArgs.cam_step, mainArgs.do_interpolation, true);
				}
				else {
					interpolate_camera_path(video_world_to_camera , cameras, sampling_rate);
					outputVideoPath = output_folder + "/video.mp4";
				}
			}
			else if (batch_mode)
			{
				computing_depth = true;
				selected_camera = 0;
				depth_maps.clear();
				depth_maps.resize(cameras.size());
			}
		}
		catch (const std::exception &e)
		{
			std::cerr << "Failed to load scene: " << e.what() << std::endl;
		}
	}

	SIBR_LOG << "Start rendering at frame " << startFrame + 1 << "/" << cameras.size() << std::endl;
	std::vector<cv::Mat3b> _rendered_frames;
	// Main loop
	while (true) //!glfwWindowShouldClose(window))
	{
		
		
		if (compute_single_depth_map || computing_depth)
		{
			cv::Mat1f depth_map = compute_depth_map(selected_camera, cameras, near_planes, far_planes, consensus_volumes, stereo_params);

			depth_maps[selected_camera] = depth_map;
			cameras[selected_camera].load_image(9999);
			cameras[selected_camera].clear_image();

			if (computing_depth && ++selected_camera >= static_cast<int>(cameras.size()))
			{
				selected_camera = 0;
				computing_depth = false;
				const std::string depth_folder =
					scene_directory_exists(mainArgs.dataset_path, "initial_depth") ? "final_depth" : "initial_depth";
				std::cout << "Writing depth to folder: " << depth_folder << std::endl;
				save_depth_maps(cameras, depth_maps, depth_folder, mainArgs.dataset_path);

				if (batch_mode)
				{
					building_volumes = true;
					selected_camera = 0;
					consensus_volumes.clear();
					consensus_volumes.resize(cameras.size());
					//consensus_textures.clear();
					cached_camera = -1;
				}
			}
		}


		if ((build_single_volume || building_volumes) && !depth_maps.empty())
		{
			cached_camera = -1;
			consensus_volumes[selected_camera].clear();
			build_consesus_volume(selected_camera, cameras, depth_maps, near_planes, far_planes,
				consensus_params, consensus_volumes[selected_camera]);
			if (building_volumes && ++selected_camera >= static_cast<int>(cameras.size()))
			{
				selected_camera = 0;
				building_volumes = false;
				const std::string consensus_folder =
					scene_directory_exists(mainArgs.dataset_path, "initial_consensus") ? "final_consensus" : "initial_consensus";
				std::cout << "Writing consensus to folder: " << consensus_folder << std::endl;
				save_consensus_volumes(cameras, consensus_volumes, consensus_folder, mainArgs.dataset_path);

				if (batch_mode) {
					std::cout << "Finish building " << consensus_folder << " vollume!!!" << std::endl;
					return 0;
				}
			}
		}

		int ibr_width = width;
		int ibr_height = height;

		Eigen::Affine3f world_to_camera;
		Eigen::Vector3f position;

		if (dumping_video)
		{
			world_to_camera = video_world_to_camera[current_video_frame++];
			position = (world_to_camera.inverse() *
				Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f)).head<3>();
		}

		

		if (dumping_video || render_image)
		{
			RenderParameters params;
			

			cv::Mat3f ibr_image =
				render_ibr(position, world_to_camera.matrix(),
					fov, Eigen::Vector2i(ibr_width, ibr_height),
					near_planes, far_planes, consensus_volumes, cameras,
					params);

			

			std::stringstream render_stream;
			render_stream << output_folder << "/soft3d_"
				<< std::setw(6) << std::setfill('0')
				<< current_video_frame << ".jpg";
			std::cout << "Writing image " << current_video_frame << "/" << video_world_to_camera.size() << std::endl;
			
			cv::imwrite(render_stream.str(), ibr_image);

			cv::Mat3b ibr_image_8b = ibr_image;
			
			_rendered_frames.push_back(ibr_image_8b);
			if (current_video_frame >= static_cast<int>(video_world_to_camera.size()))
			{
				sibr::FFVideoEncoder ffVdo(outputVideoPath, sampling_rate, sibr::Vector2i(ibr_width, ibr_height));
				
				for (int i = 0; i < _rendered_frames.size(); i++) {
					ffVdo << _rendered_frames[i];
				}

				dumping_video = false;
				current_video_frame = 0;
				video_world_to_camera.clear();
				if (batch_mode) {
					std::cout << "Finish Rendering!!" << std::endl;
					return 0;
				}
			}
		}
	}

	// Cleanup
	glfwTerminate();

	return 0;
}
